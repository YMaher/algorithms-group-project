import random
number_of_weights = 0
genuine_coin_weight = 10


def main():
    global genuine_coin_weight
    n = int(input("Enter number of coins: "))
    if n <= 1:
        print(f'Error: number of coins should be > 1')
        return
    coins = [genuine_coin_weight for _ in range(n)]
    differ_amount = random.randint(1, 4)
    coins[random.randint(0, n - 1)] = genuine_coin_weight + differ_amount if random.randint(0, 100) > 50 else \
        genuine_coin_weight - differ_amount
    fake_coin_index = find_fake_coin(coins, 0, n - 1)
    print(f'Number of weights to find the fake coin = {number_of_weights}')
    print(f'The fake coin is at index: {fake_coin_index}')
    print(f'The weight of the genuine coins = {genuine_coin_weight}')
    print(f'The weight of the fake coins = {coins[fake_coin_index]}')


def weight(coins, start_index, end_index):
    res = 0
    for i in range(start_index, end_index + 1):
        res += coins[i]
    return res


def get_expected_weight(coins_number):
    global genuine_coin_weight
    return coins_number * genuine_coin_weight


def find_fake_coin(coins, start_index, end_index):
    if start_index == end_index:
        return start_index
    global number_of_weights
    number_of_weights += 1
    middle = (start_index + end_index) // 2
    if weight(coins, start_index, middle) != get_expected_weight(middle - start_index + 1):
        return find_fake_coin(coins, start_index, middle)
    else:
        return find_fake_coin(coins, middle + 1, end_index)


if __name__ == '__main__':
    main()
