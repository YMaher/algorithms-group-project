flip_counter = 0


def flip_switch(bulbs, switch_index):
    global flip_counter
    flip_counter += 1
    print("[{}] Flipping switch #{}: ".format(flip_counter, switch_index + 1), end='\t')
    for i in range(-1, 2, 1):  # loops over -1, 0, 1
        index = switch_index + i

        if index >= len(bulbs):
            index -= len(bulbs)
        elif index < 0:
            index += len(bulbs)

        bulbs[index] = not bulbs[index]


def print_bulbs(bulbs):
    for bulb in bulbs:
        print('1' if bulb else '0', end=' ')
    print()


def general_algorithm(bulbs):
    global iterations
    for i in range(len(bulbs)):
        flip_switch(bulbs, i)
        print_bulbs(bulbs)
    iterations = i+1

iterations = 0
def special_case_algorithm(bulbs):
    global iterations
    for i in range(1, len(bulbs), 3):
        flip_switch(bulbs, i)
        print_bulbs(bulbs)
    iterations = (i+2) / 3


def main():
    global iterations
    bulb_count = int(input("Enter number of bulbs: "))
    if bulb_count < 3:
        print("Bulb count cannot be less than three")
    else:
        bulbs = [False for _ in range(bulb_count)]

        print()
        print("Initial bulbs' state: ")
        print_bulbs(bulbs)
        print()

        if len(bulbs) % 3 == 0:
            print("Bulb count divisible by three. Running special-case algorithm...")
            special_case_algorithm(bulbs)
        else:
            print("Bulb count not divisible by three. Running general algorithm...")
            general_algorithm(bulbs)
        print("Number of iterations is: ",iterations)


if __name__ == "__main__":
    main()
