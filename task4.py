import copy
class triangle:
    def __init__(self,baseUpwards,posTuple):
        self.baseUpwards=baseUpwards
        self.posTuple=posTuple


def addTriangles(triangleToCheck):
    global triangleCounter
    global triangles
    rowPosition=triangleToCheck.posTuple[0]
    trianglePosition=triangleToCheck.posTuple[1]
    
    #Right Triangle
    if((rowPosition,trianglePosition+1) not in triangles):
        triangles[(rowPosition,trianglePosition+1)] = triangle(not triangleToCheck.baseUpwards,(rowPosition,trianglePosition+1))
        triangleCounter+=1
    
    #Left Triangle
    if((rowPosition,trianglePosition-1) not in triangles):
        triangles[(rowPosition,trianglePosition-1)]= triangle(not triangleToCheck.baseUpwards,(rowPosition,trianglePosition-1))
        triangleCounter+=1
    
    #Base Triangle
    if(triangleToCheck.baseUpwards):
        if((rowPosition+1,trianglePosition) not in triangles):
            triangles[(rowPosition+1,trianglePosition)] = triangle(not triangleToCheck.baseUpwards,(rowPosition+1,trianglePosition))
            triangleCounter+=1
    else:
        if((rowPosition-1, trianglePosition) not in triangles):
            triangles[(rowPosition-1, trianglePosition)] = triangle (not triangleToCheck.baseUpwards, (rowPosition-1, trianglePosition))
            triangleCounter+=1            



triangles={(0,0):triangle(False,(0,0))}
triangleCounter = 1

iterationNum = int(input("Enter number of n \n"))



if iterationNum == 0:
    print("Number of Triangles: 0")
elif iterationNum <0:
    print("Invalid Iteration Number")
else:
    for _ in range(iterationNum-1):
        triCopy = copy.deepcopy(triangles)
        for key in triCopy.keys():
            addTriangles(triangles[key])
    print("Number of Triangles: " + str(triangleCounter))

        
   
