square = None

def main():
    size = int(input("Enter square size: "))
    if size < 3:
        print("Size cannot be less than three")
        quit()

    global square
    square = [[None for _ in range(size)] for _ in range(size)]

    solveSquareBottomUp(size)
    printSquare()

def solveSquareBottomUp(size):
    square[0][:3] = [2, 7, 6]
    square[1][:3] = [9, 5, 1]
    square[2][:3] = [4, 3, 8]

    prevSize = 3
    nextSize = prevSize*2-1
    wentOutOfBounds = False
    while not wentOutOfBounds:
        # Mirror Vertically
        for i in range(prevSize):
            for j in range(prevSize):
                newIndex = nextSize-j-1
                if newIndex < size:
                    square[i][newIndex] = square[i][j]
                else: wentOutOfBounds = True

        # Mirror Horizontally
        for i in range(prevSize):
            for j in range(nextSize):
                newIndex = nextSize-i-1
                if newIndex < size and j < size:
                    square[newIndex][j] = square[i][j]
                else: wentOutOfBounds = True

        # Mirror the bigger square on the next iteration
        prevSize = nextSize
        nextSize = prevSize*2-1

def printSquare():
    for i in square:
        print("[ ", end="")
        for j in i:
            print("-" if j == None else j,end=" ")
        print("]")
    print()

def leastDivisbleByTwo():
    temp = 1
    
if __name__ == "__main__":
    main()