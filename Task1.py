class stick:
    def __init__ (self,length):
        self.length=int(length)

def calculateNumberOfCuts(CurrentSticks):
    global CurrentNumberofSticks
    global CurrentNumberOfCuts
    global NumberOfSticks
    while (CurrentNumberofSticks != NumberOfSticks):
        x = len(CurrentSticks)
        for i in range (x):
            if (CurrentSticks[i].length > 1):
                if ((CurrentSticks[i].length) %2 ==0):
                    CurrentSticks.append(stick(int((CurrentSticks[i].length)/2)))
                elif ((CurrentSticks[i].length) %2 ==1):
                    CurrentSticks.append(stick(int((CurrentSticks[i].length)/2) +1 ))
                CurrentSticks[i].length = int(CurrentSticks[i].length /2)
                CurrentNumberofSticks += 1
        CurrentNumberOfCuts+=1
    return CurrentNumberOfCuts

length = -1
CurrentNumberofSticks = 1
while (length<= 0):
    length = int(input("Enter Length of the Stick "))  #Since n unit long stick will be cut into n pieces
    if (length<= 0):
        print ("Invalid Stick Length , please enter positive stick length\n")

NumberOfSticks = length
CurrentSticks= []
CurrentSticks.append(stick(length))
CurrentNumberOfCuts = 0
print("\nMinimum number of cuts for "+ str(NumberOfSticks) + " unit long stick is " + str(calculateNumberOfCuts(CurrentSticks)) + "\n")