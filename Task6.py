from sys import exit as sexit
from colorama import init
import random
init()

grid = None

def main():
    size = int(input("Enter size of grid: "))
    if(size <= 0):
        print("Size must be greater than one")
        sexit(0)

    global grid
    grid = [['-' for _ in range(2**size)] for _ in range(2**size)]

    choice = input("Do you want to enter the coordinates of the black block manually? Enter y for yes or anything else for no\n")
    if choice == "y":
        x = int(input("Enter x-coordinate of black block: "))
        if(x < 0 or x >= 2**size-1):
            print("Invalid x-coordinate")
            sexit(0)

        y = int(input("Enter y-coordinate of black block: "))
        if(y < 0 or y >= 2**size-1):
            print("Invalid y-coordinate")
            sexit(0)
    else:
        x = random.randint(0,int((2**size-1)/2))*2
        y = random.randint(0,int((2**size-1)/2))*2
        
    print()

    fill_block2x2(x, y, " ")

    global memoized
    memoized={}
    solve_subgrid(0,0,2**size)
    show_grid()

memoized={}
def solve_subgrid(start_x, start_y, size, custom_split_x=-1, custom_split_y=-1):
    subgrid = get_subgrid(start_x, start_y, size)
    key = tuple([item == '-' for sublist in subgrid for item in sublist])#tuple([grid[i+start_x][j+start_y] for j in range(size) for i in range(size)])
    if key in memoized:
        copy_subgrid(start_x, start_y, memoized[key])
        return

    trominoChar = getColoredTile()
    if size == 1:
        if count_blank_tiles(start_x, start_y, 1) != 1: fail()
        memoized[key] = get_subgrid(start_x, start_y, size)
    elif size == 2:
        count = count_blank_tiles(start_x, start_y, size)

        if count not in (1,4): fail()
        if count == 1:
            fill_block2x2(start_x,start_y,trominoChar)

        memoized[key] = get_subgrid(start_x, start_y, size)
    else:
        x_split = size//2 if custom_split_x == -1 else custom_split_x
        y_split = size//2 if custom_split_y == -1 else custom_split_y

        x_split += start_x
        y_split += start_y
        size //= 2
        
        count = count_blank_tiles(x_split-1, y_split-1, 2)

        if count in (0,1):
            if count_blank_tiles(start_x, start_y, size) > 0: # topleft
                fill_block(x_split, y_split, trominoChar)
                fill_block(x_split, y_split-1, trominoChar)
                fill_block(x_split-1, y_split, trominoChar)
            elif count_blank_tiles(x_split, start_y, size) > 0: # topright
                fill_block(x_split-1, y_split, trominoChar)
                fill_block(x_split-1, y_split-1, trominoChar)
                fill_block(x_split, y_split, trominoChar)
            elif count_blank_tiles(start_x, y_split, size) > 0: # bottomleft
                fill_block(x_split, y_split-1, trominoChar)
                fill_block(x_split-1, y_split-1, trominoChar)
                fill_block(x_split, y_split, trominoChar)
            elif count_blank_tiles(x_split, y_split, size) > 0: # bottomright
                fill_block(x_split-1, y_split-1, trominoChar)
                fill_block(x_split, y_split-1, trominoChar)
                fill_block(x_split-1, y_split, trominoChar)
        elif count == 4:
            pass
        else:
            fail()

        solve_subgrid(start_x, start_y, size)
        solve_subgrid(x_split, start_y, size)
        solve_subgrid(start_x, y_split, size)
        solve_subgrid(x_split, y_split, size)

        memoized[key] = get_subgrid(start_x, start_y, size*2)

def fail():
    show_grid()
    print("Solution not found!")
    sexit(0)

def count_blank_tiles(start_x, start_y, size):
    count = 0
    for i in range(size):
        for j in range(size):
            count += 1 if grid[start_x+i][start_y+j] != '-' else 0
    return count

def show_grid():
    for i in range(len(grid[0])):
        for j in range(len(grid)):
            print(grid[j][i],end=" ")
        print()
    print()

def fill_block(x, y, char='#'):
    grid[x][y] = char

def fill_block2x2(x, y, char='#'):
    for i in (0,1):
        for j in (0,1):
            if grid[x+i][y+j] == '-':
                fill_block(x+i,y+j,char)

def get_subgrid(x,y,size):
    return [[grid[x+j][y+i] for j in range(size)] for i in range(size)]
        
def copy_subgrid(x, y, subgrid):
    for i in range(len(subgrid)):
        for j in range(len(subgrid)):
            if grid[i+x][j+y] == '-':
                grid[i+x][j+y] = subgrid[j][i]

colorCounter = 0
colors = [f"\033[{code}m" for code in range(31,36)]
# colors = [Fore.BLUE, Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.]
def getColoredTile():
    global colorCounter
    res = colors[colorCounter]+"#\033[0m"
    colorCounter = (colorCounter+1)%len(colors)
    return res

if __name__ == "__main__":
    main()
